This is a demo project for collecting metrics from spring boot application using prometheus

A normal spring boot project set up using Spring initialiser

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
      </dependency>
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
      </dependency>
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
      </dependency>

After starting the application, you should be able to see the /health & /info endpoints provided by actuator. However, you wouldnt be able to access the metrics end point yet.

      ➜  prometheus-springboot-demo curl localhost:8080/health
      {"status":"UP","diskSpace":{"status":"UP","total":248990662656,"free":103337709568,"threshold":10485760}}%
      ➜  prometheus-springboot-demo curl localhost:8080/metrics
      {"timestamp":1515496618411,"status":401,"error":"Unauthorized","message":"Full authentication is required to access this resource.","path":"/metrics"}

Add the below to application.properties to access metrics link without any authentication.
    management.security.enabled=false

Other endpoint accessible are provided below

      Mapped "{[/loggers/{name:.*}],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.LoggersMvcEndpoint.get(java.lang.String)
      Mapped "{[/loggers/{name:.*}],methods=[POST],consumes=[application/vnd.spring-boot.actuator.v1+json || application/json],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.LoggersMvcEndpoint.set(java.lang.String,java.util.Map<java.lang.String, java.lang.String>)
      Mapped "{[/loggers || /loggers.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/beans || /beans.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/mappings || /mappings.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/env/{name:.*}],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EnvironmentMvcEndpoint.value(java.lang.String)
      Mapped "{[/env || /env.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/trace || /trace.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/info || /info.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/health || /health.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.HealthMvcEndpoint.invoke(javax.servlet.http.HttpServletRequest,java.security.Principal)
      Mapped "{[/autoconfig || /autoconfig.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/configprops || /configprops.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/dump || /dump.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
      Mapped "{[/heapdump || /heapdump.json],methods=[GET],produces=[application/octet-stream]}" onto public void org.springframework.boot.actuate.endpoint.mvc.HeapdumpMvcEndpoint.invoke(boolean,javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse) throws java.io.IOException,javax.servlet.ServletException
      Mapped "{[/auditevents || /auditevents.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public org.springframework.http.ResponseEntity<?> org.springframework.boot.actuate.endpoint.mvc.AuditEventsMvcEndpoint.findByPrincipalAndAfterAndType(java.lang.String,java.util.Date,java.lang.String)
      Mapped "{[/metrics/{name:.*}],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.MetricsMvcEndpoint.value(java.lang.String)
      Mapped "{[/metrics || /metrics.json],methods=[GET],produces=[application/vnd.spring-boot.actuator.v1+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()


      ➜  prometheus-springboot-demo curl localhost:8080/trace
      [{"timestamp":1515497466598,"info":{"method":"GET","path":"/mappings","headers":{"request":{"host":"localhost:8080","user-agent":"curl/7.54.0","accept":"*/*"},"response":{"X-Application-Context":"application","Content-Type":"application/vnd.spring-boot.actuator.v1+json;charset=UTF-8","Transfer-Encoding":"chunked","Date":"Tue, 09 Jan 2018 11:31:06 GMT","status":"200"}},"timeTaken":"2"}},{"timestamp":1515497454469,"info":{"method":"GET","path":"/beans","headers":{"request":{"host":"localhost:8080","user-agent":"curl/7.54.0","accept":"*/*"},"response":{"X-Application-Context":"application","Content-Type":"application/vnd.spring-boot.actuator.v1+json;charset=UTF-8","Transfer-Encoding":"chunked","Date":"Tue, 09 Jan 2018 11:30:54 GMT","status":"200"}},"timeTaken":"18"}},{"timestamp":1515497442011,"info":{"method":"GET","path":"/loggers","headers":{"request":{"host":"localhost:8080","user-agent":"curl/7.54.0","accept":"*/*"},"response":{"X-Application-Context":"application","Content-Type":"application/vnd.spring-boot.actuator.v1+json;charset=UTF-8","Transfer-Encoding":"chunked","Date":"Tue, 09 Jan 2018 11:30:42 GMT","status":"200"}},"timeTaken":"38"}}]%
      ➜  prometheus-springboot-demo curl localhost:8080/metrics
      {"mem":531725,"mem.free":463172,"processors":8,"instance.uptime":142043,"uptime":144372,"systemload.average":3.23779296875,"heap.committed":488960,"heap.init":262144,"heap.used":25787,"heap":3728384,"nonheap.committed":44064,"nonheap.init":2496,"nonheap.used":42768,"nonheap":0,"threads.peak":40,"threads.daemon":24,"threads.totalStarted":53,"threads":26,"classes":6098,"classes.loaded":6106,"classes.unloaded":8,"gc.ps_scavenge.count":6,"gc.ps_scavenge.time":52,"gc.ps_marksweep.count":3,"gc.ps_marksweep.time":174,"httpsessions.max":-1,"httpsessions.active":0,"gauge.response.loggers":40.0,"gauge.response.beans":19.0,"gauge.response.mappings":2.0,"gauge.response.trace":10.0,"gauge.response.autoconfig":13.0,"gauge.response.heapdump.json":1380.0,"gauge.response.dump":61.0,"gauge.response.health":6.0,"gauge.response.info":10.0,"gauge.response.star-star":6.0,"gauge.response.heapdump":2467.0,"counter.status.200.mappings":1,"counter.status.200.loggers":1,"counter.status.200.beans":1,"counter.status.200.info":1,"counter.status.200.heapdump.json":1,"counter.status.200.heapdump":1,"counter.status.200.health":1,"counter.status.404.star-star":1,"counter.status.200.autoconfig":1,"counter.status.200.dump":1,"counter.status.200.trace":1}%                                                                                                                         ➜  prometheus-springboot-demo

Create a new GET end point & the test class. Endpoint - /hello

    ➜  prometheus-springboot-demo curl localhost:8080/hello
    hello!%                                                                                                                                                                                                                                                     ➜  prometheus-springboot-demo

Prometheus set up. https://njalnordmark.wordpress.com/2017/05/08/using-prometheus-with-spring-boot/


Include the below dependencies on the pom to enable Prometheus.

        <!-- Hotspot JVM metrics -->
        <dependency>
             <groupId>io.prometheus</groupId>
             <artifactId>simpleclient_hotspot</artifactId>
             <version>${prometheus.version}</version>
        </dependency>

        <dependency>
             <groupId>io.prometheus</groupId>
             <artifactId>simpleclient_servlet</artifactId>
             <version>${prometheus.version}</version>
        </dependency>
        <!-- Spring Boot -->
        <dependency>
             <groupId>io.prometheus</groupId>
             <artifactId>simpleclient_spring_boot</artifactId>
             <version>${prometheus.version}</version>
        </dependency>
        <!-- The client -->
        <dependency>
             <groupId>io.prometheus</groupId>
             <artifactId>simpleclient</artifactId>
             <version>${prometheus.version}</version>
        </dependency>

Add the annotation to the spring boot entry point class.
@EnablePrometheusEndpoint

      ➜  prometheus-springboot-demo git:(master) ✗ curl localhost:8080/prometheus
      ➜  prometheus-springboot-demo git:(master) ✗

@EnableSpringBootMetricsCollector

      ➜  prometheus-springboot-demo git:(master) ✗ curl localhost:8080/prometheus
      # HELP httpsessions_max httpsessions_max
      # TYPE httpsessions_max gauge
      httpsessions_max -1.0
      # HELP httpsessions_active httpsessions_active
      # TYPE httpsessions_active gauge
      httpsessions_active 0.0
      # HELP mem mem
      # TYPE mem gauge
      mem 352322.0
      # HELP mem_free mem_free
      # TYPE mem_free gauge
      mem_free 122132.0
      # HELP processors processors
      # TYPE processors gauge
      processors 8.0
      # HELP instance_uptime instance_uptime
      # TYPE instance_uptime gauge
      instance_uptime 17636.0
      # HELP uptime uptime

DropWizard Metrics Registry to capture metrics of the application. - https://reflectoring.io/transparency-with-spring-boot/
      metrics.version 4.0.0
      <dependency>
          <groupId>io.dropwizard.metrics</groupId>
          <artifactId>metrics-core</artifactId>
          <version>${metrics.version}</version>
      </dependency>
      Spring Boot will automatically create a MetricRegistry object for you, which can be injected.
      MetricRegistry will give you access to Meter & Timer with which you can measure number of hits, time taken etc. this needs to be set up within the endpoint.

      @Service
      public class ImportantBusinessService {

        private Meter paymentsMeter;

        @Autowired
        public ImportantBusinessService(MetricRegistry metricRegistry){
          this.paymentsMeter = metricRegistry.meter("payments");
        }

        public void pay(){
          ... // do business
          paymentsMeter.mark();  
        }  

      }
